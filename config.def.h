/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;   /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
#define ICONSIZE 16   /* icon size */
#define ICONSPACING 5 /* space between icon and title */
static const char *fonts[]          = { "monospace:antialias=true:size=8",
	"JetBrains Mono:size=9:antialias=true:hinting=true",
	"JetBrainsMono Nerd Font:size=12:antialias=true:hinting=true"};
static const int statusfontindex = 1 ; //Select font of font list to use for statusbar
static const char dmenufont[]       = "monospace:antialias=true:size=8";
static const char background[]      = "#282a36";
static const char foreground[]      = "#f8f8f2";
static const char selected[]        = "#bd93f9";
static const char norm_border[]          = "#44475a";
static const char hidden_fg[]         = "#6272a4";
static const char *colors[][4]      = {
	/*               fg          bg          border   */
	[SchemeNorm] = { foreground, background, norm_border },
	[SchemeSel]  = { foreground, selected,   selected    },
	[SchemeHid]  = { hidden_fg,  norm_border, norm_border },
	[SchemeOpen] = { foreground, norm_border, selected    },
};

static const char *const autostart[] = {
	"dwmblocks", NULL,
	"picom", NULL,
	"sh", "-c", "feh --bg-scale ~/.config/.background", NULL,
	"sh", "-c",
	"if (xrdb -query | grep -q \"^dwm\\\\.started:\\\\s*true$\"); then exit; fi;"
	"echo 'dwm.started:true' | xrdb -merge;"
	"dex --environment dwm --autostart", NULL,
	"cbatticon", NULL,
	NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "dev", "www", "sys", "doc", "vm", "chat", "class", "fics", "games", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   isterminal   noswallow  monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           0,           0,          -1 },
	{ "splash",   NULL,       NULL,       0,            1,           0,           0,          -1 },
	{ "mpv",      NULL,       NULL,       0,            1,           0,           0,          -1 },
	{ "jetbrains-idea",NULL, "splash",    1 << 0,       1,           0,           0,          -1 },
	{ "zoom",     NULL,       NULL,       1 << 5,       1,           0,           0,          -1 },
	{ "firefox",  NULL,       NULL,       1 << 7,       0,           0,           0,          -1 },
	{ "St",       NULL,       NULL,       0,            0,           1,           0,          -1 },
	{ "Alacritty",NULL,       NULL,       0,            0,           1,           0,          -1 },
	{ "Brave-browser",NULL,   NULL,       1 << 1,       0,           0,           0,          -1 },
	{ "discord",  NULL,       NULL,       1 << 5,       0,           0,           0,          -1 },
	{ "teams-for-linux",NULL, NULL,	      1 << 6,     0,           0,           0,          -1 },
	{ NULL,			NULL,     "Event Tester", 0,        0,           0,           1,          -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "H[]",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "HHH",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", background, "-nf", foreground, "-sb", selected, "-sf", foreground, NULL };
static const char *powermenucmd[] = { "dmenu_power_menu", "-fn", dmenufont, "-nb", background, "-nf", foreground, "-sb", selected, "-sf", foreground, NULL };
static const char *termcmd[]  = { "st", NULL };

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_r,      spawn,          SHCMD("rofi -show drun") },
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          SHCMD("pcmanfm") },
	{ ControlMask|ShiftMask,        XK_Escape, spawn,          SHCMD("st htop") },
	{ MODKEY,                       XK_b,      spawn,          SHCMD("brave") },
	{ MODKEY,                       XK_v,      spawn,          SHCMD("st pulsemixer") },
	{ MODKEY,                       XK_e,      spawn,          SHCMD("emacs -c -a 'emacs'") },
	{ MODKEY|ControlMask,           XK_space,  togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_j,      focusstackvis,  {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstackvis,  {.i = -1 } },
	{ MODKEY|ControlMask,           XK_j,      focusstackhid,  {.i = +1 } },
	{ MODKEY|ControlMask,           XK_k,      focusstackhid,  {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_z,      zoom,           {0} },
	{ MODKEY|Mod1Mask,              XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_0,      togglegaps,     {0} },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,		                XK_space,  cyclelayout,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_space,  cyclelayout,    {.i = -1 } },
	{ MODKEY,                       XK_t,      togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_minus,  view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_minus,  tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_s,      show,           {0} },
	{ MODKEY|ShiftMask,             XK_s,      showall,        {0} },
	{ MODKEY,                       XK_n,      hide,           {0} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	TAGKEYS(                        XK_0,                      9)
	{ 0,                     XF86XK_AudioMute, spawn,          SHCMD("amixer -q sset Master toggle; kill -44 $(pidof dwmblocks)") },
	{ 0,              XF86XK_AudioRaiseVolume, spawn,          SHCMD("amixer -q sset Master 5%+; kill -44 $(pidof dwmblocks)") },
	{ 0,              XF86XK_AudioLowerVolume, spawn,          SHCMD("amixer -q sset Master 5%-; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY,                       XK_q,      quit,           {1} }, 
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = powermenucmd } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button1,        togglewin,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
